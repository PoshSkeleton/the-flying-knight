;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 1
Scriptname QF_TFKASLALStartQuest_0700AA03 Extends Quest Hidden

;BEGIN ALIAS PROPERTY TFKFlyingKnightTavern
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_TFKFlyingKnightTavern Auto
;END ALIAS PROPERTY

;BEGIN ALIAS PROPERTY TFKSerKnightsTomb
;ALIAS PROPERTY TYPE ReferenceAlias
ReferenceAlias Property Alias_TFKSerKnightsTomb Auto
;END ALIAS PROPERTY

;BEGIN FRAGMENT Fragment_0
Function Fragment_0()
;BEGIN CODE
Player.EquipItem(ClothesFarmBoots03, abSilent = true)
Player.EquipItem(ClothesFarmClothes03, abSilent = true)
Player.EquipItem(ClothesFarmHat03, abSilent = true)
Player.AddItem(LItemWeaponAny1HTown, abSilent = true)
Player.AddItem(LootCitizenPocketsCommon, abSilent = true)
Player.AddItem(LootCitizenPocketsCommon, abSilent = true)
Player.AddItem(LootCitizenPocketsCommon, abSilent = true)
Player.AddItem(Gold001, Utility.RandomInt(100,250), true)
Player.MoveTo(TFKFlyingKnightTavern.GetReference())
SetObjectiveDisplayed(10)
ARTHLALRumorsOfWarQuest.RegisterForSingleUpdate(0.25)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Armor Property ClothesFarmBoots03  Auto  

Armor Property ClothesFarmClothes03  Auto  

Armor Property ClothesFarmHat03  Auto  

LeveledItem Property LItemWeaponAny1HTown  Auto  

LeveledItem Property LootCitizenPocketsCommon  Auto  

MiscObject Property Gold001  Auto  

ReferenceAlias Property TFKFlyingKnightTavern  Auto  

Actor Property Player  Auto   

Quest Property ARTHLALRumorsOfWarQuest  Auto  
